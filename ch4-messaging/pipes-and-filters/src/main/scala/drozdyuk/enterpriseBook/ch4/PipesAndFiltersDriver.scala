package drozdyuk.enterpriseBook.ch4

import akka.actor._
import drozdyuk.enterpriseBook.ch4.filters.{Authenticator, Decrypter, Deduplicator}

object PipesAndFiltersDriver extends App {
  val orderText = "(encryption)(certificate)<order id='123'>xbc</order>"
  val orderText2 = "(encryption)(certificate)<order id='234'>xbcd</order>"

  val rawOrderBytes = orderText.toCharArray.map(_.toByte)
  val rawOrderBytes2 = orderText2.toCharArray.map(_.toByte)

  val system = ActorSystem("system")
  val filter5 = system.actorOf(Props[OrderManagementSystem], "orderMgmntSystem")
  val filter4 = system.actorOf(Props(classOf[Deduplicator], filter5), "deduplicator")
  val filter3 = system.actorOf(Props(classOf[Authenticator], filter4), "authenticator")
  val filter2 = system.actorOf(Props(classOf[Decrypter], filter3), "decrypter")
  val filter1 = system.actorOf(Props(classOf[OrderAcceptanceEndpoint], filter2), "orderAccEndpoint")

  val input = filter1
  input ! rawOrderBytes
  input ! rawOrderBytes
  input ! rawOrderBytes2

  // ctrl-c to shutdown the system
}
