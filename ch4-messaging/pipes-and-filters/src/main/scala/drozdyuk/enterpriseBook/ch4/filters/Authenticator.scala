package drozdyuk.enterpriseBook.ch4.filters

import akka.actor._
import drozdyuk.enterpriseBook.ch4.ProcessIncomingOrder

class Authenticator(nextFilter: ActorRef) extends Actor {
  def receive = {
    case message: ProcessIncomingOrder =>
      val text = new String(message.orderInfo)
      println(s"Authenticator authenticating: $text")
      val orderText = text.replace("(certificate)", "")
      nextFilter ! ProcessIncomingOrder(orderText.toCharArray.map(_.toByte))
  }
}
