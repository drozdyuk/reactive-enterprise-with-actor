package drozdyuk.enterpriseBook.ch4.filters

import akka.actor._
import drozdyuk.enterpriseBook.ch4.ProcessIncomingOrder

class Decrypter(nextFilter: ActorRef) extends Actor {
  def receive = {
    case message: ProcessIncomingOrder =>
      val text = new String(message.orderInfo)
      println(s"Decrypter decrypting: $text")
      val orderText = text.replace("(encryption)", "")
      nextFilter ! ProcessIncomingOrder(orderText.toCharArray.map(_.toByte))
  }
}
