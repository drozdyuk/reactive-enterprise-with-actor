package drozdyuk.enterpriseBook.ch4

import akka.actor._

class OrderAcceptanceEndpoint(nextFilter: ActorRef) extends Actor {
  def receive = {
    case rawOrder: Array[Byte] =>
      val text = new String(rawOrder)
      println(s"OrderAcceptanceEndpoint processing: $text.")

      nextFilter ! ProcessIncomingOrder(rawOrder)
  }
}
