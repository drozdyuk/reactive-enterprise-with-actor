package drozdyuk.enterpriseBook.ch4.filters

import akka.actor._
import drozdyuk.enterpriseBook.ch4.ProcessIncomingOrder

class Deduplicator(nextFilter: ActorRef) extends Actor {
  var ids:Set[String] = Set.empty

  def orderIdFrom(orderText: String): String = {
      val orderIdIndex = orderText.indexOf("id='") + 4
      val orderIdLastIndex = orderText.indexOf("'", orderIdIndex)
      orderText.substring(orderIdIndex, orderIdLastIndex)
  }

  def receive = {
    case message:ProcessIncomingOrder =>
      val text = new String(message.orderInfo)
      val orderId = orderIdFrom(text)
      if (ids.contains(orderId)) {
        println(s"Deduplicator excluding duplicate: $orderId")
      }
      else {
        ids = ids + orderId
        nextFilter ! message
      }
  }
}
