package drozdyuk.enterpriseBook.ch4

import akka.actor._

class OrderManagementSystem extends Actor {
  def receive = {
    case message:ProcessIncomingOrder =>
      val text = new String(message.orderInfo)

      println(s"OrderManagementSystem processing: $text")
  }
}
