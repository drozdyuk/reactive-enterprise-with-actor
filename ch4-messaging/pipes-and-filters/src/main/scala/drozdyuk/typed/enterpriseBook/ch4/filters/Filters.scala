package drozdyuk.typed.enterpriseBook.ch4.filters

import akka.typed.ScalaDSL.Static
import akka.typed.{Behavior, ActorRef}
import drozdyuk.enterpriseBook.ch4.ProcessIncomingOrder

object Filters {

  def orderAcceptanceEndpoint(nextFilter: ActorRef[ProcessIncomingOrder]): Behavior[Array[Byte]] =
    Static {
      case rawOrder =>
        val text = new String(rawOrder)
        println(s"OrderAcceptanceEndpoint processing: $text.")

        nextFilter ! ProcessIncomingOrder(rawOrder)
    }

  def authenticator(nextFilter: ActorRef[ProcessIncomingOrder]): Behavior[ProcessIncomingOrder] =
    Static {
      case message =>
        val text = new String(message.orderInfo)
        println(s"Authenticator authenticating: $text")
        val orderText = text.replace("(certificate)", "")
        nextFilter ! ProcessIncomingOrder(orderText.toCharArray.map(_.toByte))
    }

  def decrypter(nextFilter: ActorRef[ProcessIncomingOrder]): Behavior[ProcessIncomingOrder] =
    Static {
      case message =>
        val text = new String(message.orderInfo)
        println(s"Decrypter decrypting $text")
        val orderText = text.replace("(encryption)", "")
        nextFilter ! ProcessIncomingOrder(orderText.toCharArray.map(_.toByte))
    }

  def deduplicator(nextFilter: ActorRef[ProcessIncomingOrder]): Behavior[ProcessIncomingOrder] = {
    var ids: Set[String] = Set.empty
    def orderIdFrom(orderText: String): String = {
      val orderIdIndex = orderText.indexOf("id='") + 4
      val orderIdLastIndex = orderText.indexOf("'", orderIdIndex)
      orderText.substring(orderIdIndex, orderIdLastIndex)
    }

    Static {
      case message =>
        val text = new String(message.orderInfo)
        val orderId = orderIdFrom(text)
        if (ids.contains(orderId)) {
          println(s"Deduplicator excluding duplicate: $orderId")
        }
        else {
          ids = ids + orderId
          nextFilter ! message
        }
    }
  }

  val orderManagementSystem: Behavior[ProcessIncomingOrder] = {
    Static {
      case message =>
        val text = new String(message.orderInfo)
        println(s"OrderManagementSystem processing: $text")
    }
  }
}