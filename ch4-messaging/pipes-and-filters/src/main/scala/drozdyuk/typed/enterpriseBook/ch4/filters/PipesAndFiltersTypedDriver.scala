package drozdyuk.typed.enterpriseBook.ch4.filters

import akka.typed.ScalaDSL.{ContextAware, Static}
import akka.typed.{ActorSystem, Behavior, Props}


object PipesAndFiltersTypedDriver extends App {
  val pipesAndFiltersTypedDriver: Behavior[Array[Byte]] =
    ContextAware[Array[Byte]] {
      context =>
        val filter5 = context.spawn(Props(Filters.orderManagementSystem), "orderMgmntSystem")
        val filter4 = context.spawn(Props(Filters.deduplicator(filter5)), "deduplicator")
        val filter3 = context.spawn(Props(Filters.authenticator(filter4)), "authenticator")
        val filter2 = context.spawn(Props(Filters.decrypter(filter3)), "decrypter")
        val filter1 = context.spawn(Props(Filters.orderAcceptanceEndpoint(filter2)), "orderAccEndpoint")
      Static {
        case message => filter1 ! message
      }
    }

  val orderText = "(encryption)(certificate)<order id='123'>xbc</order>"
  val orderText2 = "(encryption)(certificate)<order id='234'>xbcd</order>"

  val rawOrderBytes = orderText.toCharArray.map(_.toByte)
  val rawOrderBytes2 = orderText2.toCharArray.map(_.toByte)

  val system: ActorSystem[Array[Byte]] = ActorSystem("system", Props(pipesAndFiltersTypedDriver))

  system ! rawOrderBytes
  system ! rawOrderBytes
  system ! rawOrderBytes2
  // ctrc-c to shutdown the system
}
